import boto3
region = 'ap-south-1'
ec2 = boto3.client('ec2', region_name=region)
response = ec2.describe_instances(Filters=[
        {
            'Name': 'tag:Name',
            'Values': [
                'Webserver',
            ]
        },
    ])

instances = []


for reservation in response["Reservations"]:
    for instance in reservation["Instances"]:
        instances.append(( instance["State"]["Code"], instance["InstanceId"]))

print(instances)

def lambda_handler_stop(event, context):
    instanceId=None
    for tup in instances:
        if tup[0]==16:
            instanceId=tup[1]
            break
    if instanceId:
        ec2.stop_instances(InstanceIds=[instanceId])
        print('stopped your instances: ' + str(instanceId))
    else:
        print('No Instance Running')
  
def lambda_handler_start(event, context):
    instanceId=None
    for tup in instances:
        if tup[0]==80:
            instanceId=tup[1]
            break
    if instanceId:
        ec2.start_instances(InstanceIds=[instanceId])
        print('Started instances: ' + str(instanceId))
    else:
        print('No stopped Instance available')
