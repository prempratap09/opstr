variable "aws_region" {
   description = "AWS Region to launch servers"
   default = "ap-south-1"
}

variable "aws_access_key" {
   description = "AWS User Access Key"
   default = "XXXXXXXX"
}

variable "aws_secret_key" {
   description = "AWS User Secret Key"
   default = "XXXXXXXXXXXXX"
}
variable "aws_amis" {
   default = {
       ap-south-1 = "ami-068257025f72f470d"
       ap-southeast-1 = "ami-02ee763250491e04a"
   }
}

variable "instance_type" {
   description = "Type of AWS EC2 instance."
   default     = "t2.micro"
}
variable "public_key_path" {
   description = "Enter the path to the SSH Public Key to add to AWS."
   default     = "C:\\Users\\Admin\\Downloads\\aws-key\\nginx.pem"
}
variable "key_name" {
   description = "AWS key name"
   default     = "nginx"
}

variable "instance_count" {
   default = 2
}
